console.log("Hello World");

let userInputNum = prompt("Enter a number:");

for(let i = userInputNum; i > 0; i--) {

	if(i <= 50) {
		break;
	}

	if(i % 10 == 0) {
		console.log(i + " is being skipped.");
		continue;
	}

	if(i % 5 == 0) {
		console.log(i);
	}

}

let longWord = "supercalifragilisticexpialidocious";
let consLongWord = "";

for(let i = 0; i < longWord.length; i++) {
	
	if (longWord[i]=="a" || 
		longWord[i]=="e" || 
		longWord[i]=="i" || 
		longWord[i]=="o" || 
		longWord[i]=="u") 
	{
		continue;
	} else {
		consLongWord += longWord[i];
	}
}

console.log(consLongWord);